FROM python:3.4-slim

RUN mkdir /app
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app

ENTRYPOINT ["python"]
CMD ["app.py"]
