variable "aws_region" {
    type = "string"
    description = "Default region for provider AWS"
    default = "ap-southeast-1"
}

variable "s3_bucket_name" {
    type = "string"
    description = "S3_bucket name"
    default = "qi-test-bucket-XXX"
}

