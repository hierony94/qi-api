provider "aws" {
    region = "${var.aws_region}"
    profile = "terraform"
}

resource "aws_s3_bucket" "s3bucket" {
  bucket = "qi-test-bucket"
  acl    = "private"

  tags = {
    Name = "qi-bucket"
  }
}

resource "aws_iam_user" "s3user" {
  name = "s3-bucket-access"
}

resource "aws_iam_access_key" "s3key" {
  user = "${aws_iam_user.s3user.name}"
}

resource "aws_iam_user_policy" "s3policy" {
  name = "s3-list-upload-delete"
  user = "${aws_iam_user.s3user.name}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.s3bucket.name}*"
        }
    ]
}
EOF
}
