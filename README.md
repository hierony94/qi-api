# QI-API

Python REST API using flask to manage S3 bucket (View, Delete, Upload)

## A. Pre-requisites

Docker 18.09 : http://docs.docker.com/installation/

docker-compose 1.18.0 : https://github.com/docker/compose/releases

Terraform v0.11.8 : https://www.terraform.io/ (**Optional**, If you want to provision S3 Bucket, IAM user & Policy using Terraform)

## B. Provision S3 Bucket, IAM User & Policy using Terraform
1. Clone this repo by run `git clone https://gitlab.com/hierony94/qi-api.git` move to Terraform directory `cd qi-api/terraform`
2. Create a new profile named **terraform** in AWS Credential file `.aws/credentials`
   ```
   [terraform]
    aws_access_key_id = xxxxxxxxxxxxxxxxxxx
    aws_secret_access_key = xxx/xxxxxxxxxxxxx/xxxx
   ```
   **Note**: The AWS Credential should have `IAMFullAccess` policy
3. Run `terraform init` to initialize a working directory containing Terraform configuration files
4. Run `terraform plan` to see resource that will be managed (add, change, destroy) by Terraform
5. Run `terraform apply` to provision S3 Bucket and IAM user with needed policy

**Note:** The secret access key will be written to the terraform state file, you can supply a pgp_key instead, which will prevent the secret from being stored in plain text

## C. Deployment
1. Clone this repo by run `git clone https://gitlab.com/hierony94/qi-api.git` move to repo root directory `cd qi-api`
2. Fill out **S3_BUCKET**, **S3_KEY**, **S3_SECRET_ACCESS_KEY** in .env.example file.  
   Note: **S3_BUCKET** is the S3 bucket that you want to be retrieved, **S3_KEY** is aws_access_key_id, and **S3_SECRET_ACCESS_KEY** is aws_secret_access_key.
   The AWS access key should have this permissions.
   ```
   s3:ListBucket
   s3:PutObject
   s3:DeleteObject
   ```
3. Rename .env.example file to .env `mv .env.example .env`
4. Build docker image and run the container for the app by run `docker-compose up -d --build`
5. Access the app in http://localhost:5000

## D. Monitoring
For Monitoring, we can setup simple monitoring and alerting using Uptime robot (https://uptimerobot.com). For free plan it will check the endpoint every 5 minutes.
Here's the step to create a uptime robot monitor.

1. Create alert contact from **My Settings** menu. The alert can be send into Email, SMS, Voice Call Slack channel
2. Create new uptime robot monitor.
   - Select **HTTP(S)** for the Monitor Type field
   - Insert your server IP in URL field, ex: 10.100.10.100:5000
   - Choose alert contact to notify if the server dowwn
   - Click Create Monitor
3. Done
