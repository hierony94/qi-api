"""
Simple python API for Manage S3 bucket using flask
"""

from flask import Flask, render_template, request, redirect, url_for
from flask_bootstrap import Bootstrap
import boto3
from config import S3_BUCKET, S3_KEY, S3_SECRET

S3_RESOURCE = boto3.resource(
    "s3",
    aws_access_key_id=S3_KEY,
    aws_secret_access_key=S3_SECRET
)

APP = Flask(__name__)
Bootstrap(APP)

@APP.route('/')
def index():
    """Index, view S3 bucket files"""
    my_bucket = S3_RESOURCE.Bucket(S3_BUCKET)
    summaries = my_bucket.objects.all()

    return render_template('index.html', my_bucket=my_bucket, files=summaries)

@APP.route('/upload', methods=['POST'])
def upload():
    """For uploading file to S3 bucket"""
    file = request.files['file']

    my_bucket = S3_RESOURCE.Bucket(S3_BUCKET)
    my_bucket.Object(file.filename).put(Body=file)

    return redirect(url_for('index'))

@APP.route('/delete', methods=['POST'])
def delete():
    """For deleting file in S3 bucket"""
    key = request.form['key']

    my_bucket = S3_RESOURCE.Bucket(S3_BUCKET)
    my_bucket.Object(key).delete()

    return redirect(url_for('index'))

if __name__ == '__main__':
    APP.run(debug=True, host='0.0.0.0')
